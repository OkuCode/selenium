'''Sending keys and Css selector'''

from selenium import webdriver

# Manipulate keys with this module
from selenium.webdriver.common.keys import Keys

driver_chrome = webdriver.Chrome()
driver_chrome.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')
driver_chrome.implicitly_wait(8)

try:
    btn_no = driver_chrome.find_element_by_class_name('at-cm-no-button')
    btn_no.click()
except:
    print("Element not found ....")

input_one = driver_chrome.find_element_by_id('sum1')
input_two = driver_chrome.find_element_by_id('sum2')
input_one.send_keys(Keys.NUMPAD9, Keys.NUMPAD0)
input_two.send_keys(Keys.NUMPAD5, Keys.NUMPAD4)

try:
    btn_equal = driver_chrome.find_element_by_css_selector('button[onclick="return total()"]')
    btn_equal.click()
except:
    print("CSS selector not found ...")

input_message = driver_chrome.find_element_by_id('user-message')
input_message.send_keys(
    f'Remilia{Keys.SPACE}Scarlet{Keys.SPACE}Flandre{Keys.SPACE}Scarlet'
)

try:
    btn_send = driver_chrome.find_element_by_css_selector('button[onclick="showInput();"]')
    btn_send.click()
except:
    print("Element not found....")
