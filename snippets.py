import os
from selenium import webdriver

# Explicit way

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# If you need specify the downloaded location of your driver
# os.environ['PATH'] += r'/driver/location/firefoxDriver'
# driver_firefox = webdriver.Firefox()

# You don't need download the driver anymore
driver_chrome = webdriver.Chrome()
# Getting the url
driver_chrome.get('https://www.seleniumeasy.com/test/jquery-download-progress-bar-demo.html')
# Assing elements of the web page
btn_element = driver_chrome.find_element_by_id("downloadButton")
# Let's leave a little space between the loaded web page
driver_chrome.implicitly_wait(8)
# Action where it's taking by selenium
btn_element.click()

'''
Automation

'''

# Explicit way by classes such as Webdriver, expected contiditon and by  
WebDriverWait(driver_chrome, 30).until(
    EC.text_to_be_present_in_element(
        #Element Filtration
        (By.CLASS_NAME, 'progress-label'),
        #Expected text
        'Completed!'
    )
)

