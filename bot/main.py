'''
First proyect bot using selenium

'''

from booking.booking import Booking

# Using a context manager

if __name__ == '__main__':
    with Booking() as bot:
        bot.first_page()
        bot.change_currency(currency='USD')
        bot.place_to_go('Cartagena')
        bot.date_schedule(
            date_in = '2021-09-18',
            date_out = '2021-10-01'
        )

        bot.total_people(10)
        bot.submit_search()
