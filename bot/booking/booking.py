'''Designing the bot'''

# Import selenium
from selenium import webdriver

# Import package
import booking.constant as const

# Creating a inherit class
class Booking(webdriver.Chrome):
    def __init__(self, teardown=False):
       self.teardown = teardown
       super(Booking, self).__init__()
       self.implicitly_wait(15)
       self.maximize_window()
 
    # Close the browser
    def __exit__(self, *args):
        if self.teardown:
            self.quit()

    # Get the page
    def first_page(self):
        self.get(const.PAGE)

    # Click the button 
    def change_currency(self, currency=None):
        currency_element = self.find_element_by_css_selector(
            'button[data-tooltip-text="Choose your currency"]'
        )

        currency_element.click()

        selected_currency_element = self.find_element_by_css_selector(
            # Specify the currency has be selected
            f'a[data-modal-header-async-url-param *= "selected_currency={currency}"]'
        )

        selected_currency_element.click()

    
    # Place to go
    def place_to_go(self, place=None):
        selected_place = self.find_element_by_id('ss')
        selected_place.send_keys(place)
        drop_element = self.find_element_by_css_selector(
            'li[data-i="0"]'
        )

        drop_element.click()


    # Schedule Date
    def date_schedule(self, date_in=None, date_out=None):
        check_in = self.find_element_by_css_selector(
            f'td[data-date="{date_in}"]'
        )

        check_in.click()

        button_next_element = self.find_element_by_css_selector(
            'div[data-bui-ref="calendar-next"]'
        )

        button_next_element.click()

        check_out= self.find_element_by_css_selector(
            f'td[data-date="{date_out}"]'
        )

        check_out.click()


    # People to go

    def total_people(self, count=None):
        toggle_people = self.find_element_by_id('xp__guests__toggle')
        toggle_people.click()

        while True:
            decrease_element = self.find_element_by_css_selector(
                'button[aria-label="Decrease number of Adults"]'
            )

            decrease_element.click()

            adults_element = self.find_element_by_id("group_adults") 
            # Receive html values
            adults_value = adults_element.get_attribute('value') # Adults count
            if int(adults_value) == 1:
                break
            
        
        increase_element = self.find_element_by_css_selector(
            'button[aria-label="Increase number of Adults"]'
        )

        for _ in range(count - 1):
            increase_element.click()

    # Summit your search
    def submit_search(self):
        submit_button = self.find_element_by_css_selector(
            'button[type="submit"]'
        )

        submit_button.click()
